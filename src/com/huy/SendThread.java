package com.huy;

import javax.net.ssl.SSLSocket;
import java.io.*;

import static com.huy.InputThread.copy;

public class SendThread implements Runnable {
    SSLSocket socket;
    File file;
    String line;
    int sleep;

    public SendThread(SSLSocket socket, File file, String line) {
        this.socket = socket;
        this.file = file;
        this.line = line;
    }

    @Override
    public void run() {
            try {
                System.out.println("Sending " + file);
                long length = file.length();

                if (length < 5E6) sleep = 200;
                else if (length > 5E6 && length < 1E7) sleep = 500;
                else if (length > 1E7 && length < 1.5E8) sleep = 800;
                else if (length > 1.5E8) sleep = 1600;

                InputStream in = new FileInputStream(file);
                OutputStream out = socket.getOutputStream();

                copy(in, out);
                System.out.println("Sent " + file + " (" + length + " bytes)");

                Thread.sleep(sleep);
                out.close();
                in.close();
                this.socket.close();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
    }
}
