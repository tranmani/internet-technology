package com.huy;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.*;

public class Main {
    final static String SERVER_ADDRESS = "localhost";
    final static int SERVER_PORT = 1337;
    final static int SERVER_PORT_SEND = 6969;
    public static void main(String[] args) throws IOException {
        SSLSocket socket = new SSLSocket(SERVER_ADDRESS, SERVER_PORT) {
            @Override
            public String[] getSupportedCipherSuites() {
                return new String[0];
            }

            @Override
            public String[] getEnabledCipherSuites() {
                return new String[0];
            }

            @Override
            public void setEnabledCipherSuites(String[] strings) {

            }

            @Override
            public String[] getSupportedProtocols() {
                return new String[0];
            }

            @Override
            public String[] getEnabledProtocols() {
                return new String[0];
            }

            @Override
            public void setEnabledProtocols(String[] strings) {

            }

            @Override
            public SSLSession getSession() {
                return null;
            }

            @Override
            public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

            }

            @Override
            public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

            }

            @Override
            public void startHandshake() throws IOException {

            }

            @Override
            public void setUseClientMode(boolean b) {

            }

            @Override
            public boolean getUseClientMode() {
                return false;
            }

            @Override
            public void setNeedClientAuth(boolean b) {

            }

            @Override
            public boolean getNeedClientAuth() {
                return false;
            }

            @Override
            public void setWantClientAuth(boolean b) {

            }

            @Override
            public boolean getWantClientAuth() {
                return false;
            }

            @Override
            public void setEnableSessionCreation(boolean b) {

            }

            @Override
            public boolean getEnableSessionCreation() {
                return false;
            }
        };

        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();

        InputThread readMessage = new InputThread(socket, inputStream);
        Thread t1 = new Thread(readMessage);
        OutputThread sendMessage = new OutputThread(socket, outputStream);
        Thread t2 = new Thread(sendMessage);

        t1.start();
        t2.start();
    }
}