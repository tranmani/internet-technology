package com.huy;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.*;

public class OutputThread implements Runnable {
    SSLSocket socket;
    PrintWriter writer;
    BufferedReader reader = null;
    // secondPartString in SEND protocol will be file's name
    static String secondPartString;

    public OutputThread(SSLSocket socket, OutputStream outputstream) {
        this.socket = socket;
        this.writer = new PrintWriter(outputstream);
    }

    public void run() {
        try {
            System.out.println("Type [HELO your_username] before using other protocols, type 'QUIT' to disconnect from server");
            while (true) {
                reader = new BufferedReader(new InputStreamReader(System.in));
                String line = reader.readLine();
                String userTyped = extractString(line);
                secondPartString = extractString(userTyped);

                if (checkMessageType(line).equals("SEND")) {
                    writer.println(line);
                    writer.flush();
                    Thread.sleep(5);
                    if (!InputThread.line.contains("-ERR")) {
                        String filePath = "C://" + secondPartString;
                        File file = new File(filePath);

                        SSLSocket socket2 = new SSLSocket(Main.SERVER_ADDRESS, Main.SERVER_PORT_SEND) {
                            @Override
                            public String[] getSupportedCipherSuites() {
                                return new String[0];
                            }

                            @Override
                            public String[] getEnabledCipherSuites() {
                                return new String[0];
                            }

                            @Override
                            public void setEnabledCipherSuites(String[] strings) {

                            }

                            @Override
                            public String[] getSupportedProtocols() {
                                return new String[0];
                            }

                            @Override
                            public String[] getEnabledProtocols() {
                                return new String[0];
                            }

                            @Override
                            public void setEnabledProtocols(String[] strings) {

                            }

                            @Override
                            public SSLSession getSession() {
                                return null;
                            }

                            @Override
                            public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

                            }

                            @Override
                            public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

                            }

                            @Override
                            public void startHandshake() throws IOException {

                            }

                            @Override
                            public void setUseClientMode(boolean b) {

                            }

                            @Override
                            public boolean getUseClientMode() {
                                return false;
                            }

                            @Override
                            public void setNeedClientAuth(boolean b) {

                            }

                            @Override
                            public boolean getNeedClientAuth() {
                                return false;
                            }

                            @Override
                            public void setWantClientAuth(boolean b) {

                            }

                            @Override
                            public boolean getWantClientAuth() {
                                return false;
                            }

                            @Override
                            public void setEnableSessionCreation(boolean b) {

                            }

                            @Override
                            public boolean getEnableSessionCreation() {
                                return false;
                            }
                        };
                        SendThread sendThread = new SendThread(socket2, file, line);
                        Thread t = new Thread(sendThread);
                        t.start();
                    }
                }
                if (checkMessageType(line).equals("UNKNOWN")) {
                    writer.println(line);
                    writer.flush();
                }
                if (checkMessageType(line).equals("QUIT")) {
                    writer.println(line);
                    writer.flush();
                    break;
                }
            }
            this.socket.close();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
        }
    }

    private String extractString(String mess) {
        return mess.substring(mess.indexOf(" ") + 1);
    }

    private String checkMessageType(String mess) {
        String result = "UNKNOWN";
        if (mess.length() > 0) {
            String type = mess.substring(0, 4);
            if (type.equals("SEND")) {
                result = "SEND";
            } else if (type.equals("QUIT")) {
                result = "QUIT";
            }
        }
        return result;
    }
}
