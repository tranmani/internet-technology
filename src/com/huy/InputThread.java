package com.huy;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.*;

public class InputThread implements Runnable {
    BufferedReader reader;
    static String line = null;
    SSLSocket socket;
    PrintWriter writer;

    public InputThread(SSLSocket socket, InputStream inputStream) {
        this.socket = socket;
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public void run() {
        try {
            while ((line = reader.readLine()) != null) {
                if (line.contains("]: Send you a file")) {
                    SSLSocket socket2 = new SSLSocket(Main.SERVER_ADDRESS, Main.SERVER_PORT_SEND) {
                        @Override
                        public String[] getSupportedCipherSuites() {
                            return new String[0];
                        }

                        @Override
                        public String[] getEnabledCipherSuites() {
                            return new String[0];
                        }

                        @Override
                        public void setEnabledCipherSuites(String[] strings) {

                        }

                        @Override
                        public String[] getSupportedProtocols() {
                            return new String[0];
                        }

                        @Override
                        public String[] getEnabledProtocols() {
                            return new String[0];
                        }

                        @Override
                        public void setEnabledProtocols(String[] strings) {

                        }

                        @Override
                        public SSLSession getSession() {
                            return null;
                        }

                        @Override
                        public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

                        }

                        @Override
                        public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {

                        }

                        @Override
                        public void startHandshake() throws IOException {

                        }

                        @Override
                        public void setUseClientMode(boolean b) {

                        }

                        @Override
                        public boolean getUseClientMode() {
                            return false;
                        }

                        @Override
                        public void setNeedClientAuth(boolean b) {

                        }

                        @Override
                        public boolean getNeedClientAuth() {
                            return false;
                        }

                        @Override
                        public void setWantClientAuth(boolean b) {

                        }

                        @Override
                        public boolean getWantClientAuth() {
                            return false;
                        }

                        @Override
                        public void setEnableSessionCreation(boolean b) {

                        }

                        @Override
                        public boolean getEnableSessionCreation() {
                            return false;
                        }
                    };
                    ReceiveThread receiveThread = new ReceiveThread(socket2, extractFileName(line));
                    Thread t1 = new Thread(receiveThread);
                    t1.start();
                }

                if (line.equals("PING")) {
                    this.writer = new PrintWriter(socket.getOutputStream());
                    writer.println("PONG");
                    writer.flush();
                } else {
                    System.out.println("*Server*: " + line);
                }
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int copy(InputStream in, OutputStream out) throws IOException {
        byte[] buf = new byte[2048];
        int bytesRead = 0;
        int totalBytes = 0;
        while((bytesRead = in.read(buf)) != -1) {
            totalBytes += bytesRead;
            out.write(buf, 0, bytesRead);
        }
        return totalBytes;
    }

    public String extractFileName(String mess) {
        mess = mess.substring(mess.indexOf("(") + 1);
        mess = mess.substring(0, mess.indexOf(")"));
        return mess;
    }
}