package com.huy;

import javax.net.ssl.SSLSocket;
import java.io.*;

import static com.huy.InputThread.copy;

public class ReceiveThread implements Runnable {
    SSLSocket socket;
    String fileName;

    public ReceiveThread(SSLSocket socket, String fileName) {
        this.socket = socket;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        try {
            File dir = new File("C://receive/");
            dir.mkdirs();
            InputStream in = socket.getInputStream();
            OutputStream out = new BufferedOutputStream(new FileOutputStream("C://receive/" + fileName));
            int bytesCopied = copy(in, out);
            System.out.println(String.format("%d bytes received", bytesCopied));
            out.close();
            in.close();
            socket.close();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
